package model;

import java.io.Serializable;

public class Cart implements Serializable {

	private int eggkosuu;
	private int breadkosuu;

	public Cart() {
	}

	public Cart(int eggkosuu, int breadkosuu) {
		this.eggkosuu = eggkosuu;
		this.breadkosuu = breadkosuu;
	}

	public int getEggkosuu() {
		return eggkosuu;
	}

	public void setEggkosuu(int eggkosuu) {
		this.eggkosuu = eggkosuu;
	}

	public int getBreadkosuu() {
		return breadkosuu;
	}

	public void setBreadkosuu(int breadkosuu) {
		this.breadkosuu = breadkosuu;
	}

}