package model;

import java.io.Serializable;

public class Credit implements Serializable  {

	//private int goukei;
		private String num; //カードナンバー
		private String name; //名前
		private String cardMonth;
		private String cardYear;
		private String security;//セキュリティナンバー

		public Credit(){}
		public Credit(String num, String name, String cardMonth, String cardYear, String security){
			//this.goukei=goukei;
			this.num =num;
			this.name=name;
			this.cardMonth=cardMonth;
			this.cardYear=cardYear;
			this.security=security;

		}

		public String getNum() {
			return num;
		}
		public void setNum(String num) {
			this.num = num;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCardMonth() {
			return cardMonth;
		}
		public void setCardMonth(String cardMonth) {
			this.cardMonth = cardMonth;
		}
		public String getCardYear() {
			return cardYear;
		}
		public void setCardYear(String cardYear) {
			this.cardYear = cardYear;
		}
		public String getSecurity() {
			return security;
		}
		public void setSecurity(String security) {
			this.security = security;
		}



	}



