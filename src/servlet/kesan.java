package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Credit;

/**
 * Servlet implementation class kesan
 */
@WebServlet("/kesan")
public class kesan extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String num=request.getParameter("num");
		String name=request.getParameter("name");
		String cardMonth=request.getParameter("cardMonth");
		String cardYear=request.getParameter("cardYear");
		String security=request.getParameter("security");

		Credit cre=new Credit(num,name,cardMonth,cardYear,security);

		request.setAttribute("cre", cre);
		
		String errorMsg ="";
		if(num == null || num.length() ==0){
			errorMsg+="カード番号が無いでんがな。<br/>";
		}
		if(name == null || name.length() ==0){
			errorMsg+="名前が無いでんがな。<br/>";
		}
		if(security == null || security.length() ==0){
			errorMsg+="セキュリティ番号が入れといてな！<br/>";
		}

		request.setAttribute("errorMsg", errorMsg);

			//フォワード
			RequestDispatcher dispatcher=
					request.getRequestDispatcher("/WEB-INF/jsp/kesanDone.jsp");
			dispatcher.forward(request, response);

}
}
