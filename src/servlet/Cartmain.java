package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Cart;

/**
 * Servlet implementation class cartmain
 */
@WebServlet("/Cartmain")
public class Cartmain extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String eggkosuuString= request.getParameter("eggkosuu");
		String breadkosuuString= request.getParameter("breadkosuu");

		int eggkosuu=Integer.parseInt(eggkosuuString);
		int breadkosuu=Integer.parseInt(breadkosuuString);
		Cart ca=new Cart(eggkosuu,breadkosuu);

		request.setAttribute("ca", ca);

		int result=0;
		int result2=0;
		int result3=0;

		if(eggkosuu != 0){

			result = eggkosuu * 128;
		}else{
			result = 0;
		}
		if(breadkosuu != 0){
			result2 = breadkosuu *148;
		}else{
			result2 = 0;
		}
		result3=result+result2;

		request.setAttribute("result3", result3);

			//フォワード
			RequestDispatcher dispatcher=
					request.getRequestDispatcher("/WEB-INF/jsp/ketei.jsp");
			dispatcher.forward(request, response);
	}
}
