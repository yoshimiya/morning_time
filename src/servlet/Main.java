  package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Beer;
import model.PostBeerLogic;
import model.WitterUser;

/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//つぶやきリストをアプリケーションリストから取得
		ServletContext application= this.getServletContext();
		List<Beer>beerList=(List<Beer>) application.getAttribute("beerList");

		//取得出来なかった場合は、つぶやきリストを新規作成してアプリケーションスコープに保存
		if(beerList == null){
			beerList=new ArrayList<>();
			application.setAttribute("beerList", beerList);
		}
		//ログインしているか確認するため
		//セッションスコープからユーザー情報を取得
		HttpSession session =request.getSession();
		WitterUser loginUser =(WitterUser) session.getAttribute("loginUser");

		if(loginUser == null){ //ログインしてない場合
			//リダイレクト
			response.sendRedirect("/morning/witter.jsp/");
		} else { //ログイン済みの場合
			//フォワード
			RequestDispatcher dispatcher=
					request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String text = request.getParameter("text");

		//入力値チェック
		if(text != null && text.length() !=0){
			//アプリケーションスコープに保存されたつぶやきリストを取得
			ServletContext application = this.getServletContext();
			List<Beer>beerList=(List<Beer>)application.getAttribute("beerList");

			//セッションスコーンに保存されたユーザー情報を取得
			HttpSession session = request.getSession();
			WitterUser loginUser=(WitterUser)session.getAttribute("loginUser");

			//つぶやきをつぶやきリストに追加
			Beer beer=new Beer(loginUser.getName(),text);
			PostBeerLogic postBeerLogic=new PostBeerLogic();
			postBeerLogic.execute(beer, beerList);

			//アプリケーションスコープにつぶやきリストを保存
			application.setAttribute("beerList", beerList);
		}else{
		//エラーメッセージをリクエストスコープに保存
			request.setAttribute("errorMsg","つぶやきが入力されていません");
		}

		//メイン画面にフォワード
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
		dispatcher.forward(request, response);
	}

}
