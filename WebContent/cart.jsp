<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ネットショッピング</title>
<link rel="stylesheet" href="css/style4.css">
</head>
<body>
<header>
<h1>ワオンねっと　ショッピング</h1>

<img src="img/cartimage1.png" alt="カート">
</header>
<article>
<h1>ーお買い物フォームー</h1>
<form action="/morning/Cartmain" method="post">
<p><span>●</span>　取りたて 朝タマゴ　１０個：１２８円　個数　
<select name="eggkosuu">
			<option value="0">0個</option>
            <option value="1">1個</option>
            <option value="2">2個</option>
            <option value="3">3個</option>
            <option value="4">4個</option>
            <option value="5">5個</option>
            <option value="6">6個</option>
            <option value="7">7個</option>
            <option value="8">8個</option>
            <option value="9">9個</option>
            <option value="10">10個</option>
</select>
</p>
<p><span>●</span>　モチもち！ワオン食パン：１４８円　個数　
<select name="breadkosuu">
			<option value="0">0個</option>
            <option value="1">1個</option>
            <option value="2">2個</option>
            <option value="3">3個</option>
            <option value="4">4個</option>
            <option value="5">5個</option>
            <option value="6">6個</option>
            <option value="7">7個</option>
            <option value="8">8個</option>
            <option value="9">9個</option>
            <option value="10">10個</option>
</select>
</p>
<p><button type="submit">お会計</button></p>
</form>
<br/>
<a href="index.jsp">戻る</a>
<br/>
<br/>
<p>※（注）特価商品お一人様各１０個まで</p>
</article>
<footer class="copyright">
<p>copyright &copy; waon.co.jp</p>
</footer>
</body>
</html>