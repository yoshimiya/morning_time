<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ page import="model.WitterUser" %>
    <%
    //セッションスコープからユーザー情報を取得
    WitterUser loginUser=(WitterUser) session.getAttribute("loginUser");
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>witter</title>
<link rel="stylesheet" type="text/css" href="css/style3.css">
</head>
<body>
<header>
<img src="img/witter.png" alt="waon_top">
</header>

<article>

<% if(loginUser != null) {%>
	<p>ログインに成功しました</p>
	<p>ようこそ<%=loginUser.getName() %>さん</p>
	<a href="/morning/Main">つぶやき投稿・閲覧へ</a>
	<% }else{ %>
	<P>ログインに失敗しました</P>
	<a href="/morning/witter.jsp">Topへ</a>
	<%} %>

	</article>
</body>
</html>