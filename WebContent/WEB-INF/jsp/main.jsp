<%@page import="model.Beer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="model.WitterUser,model.Beer,java.util.List"%>
    <%
    WitterUser loginUser =(WitterUser) session.getAttribute("loginUser");

    List<Beer>beerList=
    	    (List<Beer>)application.getAttribute("beerList");

    String errorMsg =(String) request.getAttribute("errorMsg");
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>witter</title>
<link rel="stylesheet" type="text/css" href="css/style3.css">
</head>
<body>
<header>
<img src="img/witter.png" alt="waon_top">
</header>

<article>
<h1>うぃったー</h1>
<p><%=loginUser.getName() %>さん、ログイン中</p>
<a href="/morning/Logout">ログアウト</a>
	</p>
	<p><a href="/morning/Main">更新</a></p>
	<form action="/morning/Main" method="post">
	<input type="text" name="text">
	<input type="submit" value="つぶやく">
	</form>
	<%if(errorMsg != null) {%>
	<p><%= errorMsg %></p>
	<% } %>
	<%for(Beer beer:beerList) {%>
	<p><%=beer.getUserName() %>:<%=beer.getText() %></p>
	<% } %>
	</article>
</body>
</html>