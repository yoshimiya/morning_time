<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>おおきにクレジット</title>

<link rel="stylesheet" href="css/style.css">

</head>
<body>
<header>
<h1>毎度、おおきにクレジット</h1>
<p>
<img src="img/okini1.jpg" alt="おおきにクレジット"/>
</p>
</header>

<article>
<hr  color=#999999 width="100%" >
<p>
<span class="iro">必須</span> 利用代金の支払い方法
</p>
<p>ご利用代金は、１ヶ月分まとめて翌日６日（土・日・祝日の場合は翌営業日）にあらかじめ指定した口座から自動振替の方法により、</p>
<p>お支払いたします。クレジットカードによるお支払いの場合は、カード会社とのご契約内容に準じます。</p>
<p>※適当に記入されてください。カード情報の保存はされません。</p>
<form action="/morning/kesan" method="post">
<table>
	<tr><td colspan="2" class="kureji"><span id="maru">●</span>クレジットカード</td></tr>
	<tr><td colspan="2" class="imagecd"><img src="img/card.jpg" alt="おおきにクレジットご利用カード"/></td></tr>
	<tr>
		<th class="moji">
	カード番号 <span class="iro">必須</span>
</th>
<td class="into">半角数字<br/><input type="text" name="num" ></td>
	</tr>
	<tr>
	<th>name <span class="iro">必須</span> </th>
<td class="into">半角英文字<br/><input type="text" name="name" placeholder="TARO OOKINI"></td>
	</tr>
	<tr>
	<th>有効期限 <span class="iro">必須</span> </th>
	<td class="into">半角数字<br/>
          <select class="form-control" name="month" id="month">
            <option value="1">1月</option>
            <option value="2">2月</option>
            <option value="3">3月</option>
            <option value="4">4月</option>
            <option value="5">5月</option>
            <option value="6">6月</option>
            <option value="7">7月</option>
            <option value="8">8月</option>
            <option value="9">9月</option>
            <option value="10">10月</option>
            <option value="11">11月</option>
            <option value="12">12月</option>
          </select> /

          <select class="form-control" name="year" id="year">

			<option value="19">19年</option>
            <option value="20">20年</option>
            <option value="21">21年</option>
            <option value="22">22年</option>
            <option value="23">23年</option>
            <option value="24">24年</option>
            <option value="25">25年</option>
            <option value="26">26年</option>
            <option value="27">27年</option>
            <option value="28">28年</option>
            <option value="29">29年</option>
            <option value="30">30年</option>
            <option value="31">31年</option>
            <option value="32">32年</option>
            <option value="33">33年</option>
            <option value="34">34年</option>
            <option value="35">35年</option>
            <option value="36">36年</option>
            <option value="37">37年</option>
            <option value="38">38年</option>
            <option value="39">39年</option>
            <option value="40">40年</option>
            <option value="41">41年</option>
            <option value="42">42年</option>
            <option value="43">43年</option>
            <option value="44">44年</option>
            <option value="45">45年</option>
            <option value="46">46年</option>
            <option value="47">47年</option>
            <option value="48">48年</option>
            <option value="49">49年</option>
            <option value="50">50年</option>

          </select>
</td>
</tr>
<tr>
	<th>
	セキュリティコード <span class="iro">必須</span>
</th>
<td class="into">半角数字<br/>
<input type="password" name="security">
</td>
</tr>
<tr>
	<td colspan="2" class="btn">
	<button type="submit">（ポチッと）払うでぇ！</button>
	</td>
</tr>

</table>
	</form>
<div class="toppage">
<a href="index.jsp">WAON TOPPAGEへ戻る</a>
</div>
</article>

<footer class="copyright">
<p>copyright &copy; okini_credit.com</p>
</footer>

</body>
</html>
