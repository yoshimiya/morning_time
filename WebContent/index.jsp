<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ワオン　ショッピングモール</title>
	<link rel="stylesheet" href="css/style2.css">
  <script src="js/jquery-3.4.1.min.js"></script>
<script src="js/jquery-3.4.1.js"></script>
<script src="js/main.js"></script>

  </head>
<body>
<header>
<p>注）パロディ作品です。</p>
<h1>　ワオン　ショッピングモール</h1>

<img src="img/waon_top.jpg" alt="waon_top">

</header>
  <article>
<div class="box">
  <p>朝市セール CM</p>
  <p><img src="img/morning_title.jpg" alt="titleimage"></p>
  <h2>― Morning time / Breakfast ―</h2>
</div>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

   <div class="box1">
	   <div class="item"><p>蒼い朝、<br/>
  ベーコンの焼く香ばしい香り<br/>
  で目がさめる</p>
	</div>
	<div class="item">
  <img src="img/asa.jpg" alt="asagohan">
  </div>
  </div>
</div>
</section>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

   <div class="box2">
	   <div class="item2">
	<img src="img/morning_blue.jpg" alt="waonimage_morning">
	</div>
	  <div class="item2">
  <p>カーテンの隙間から<br/>
  青白い朝の明かりが差し込んでくる<br/>
  <br/>
  布団で舞ったホコリで<br/>
  キラキラと空気が輝いている<br/>
 <br/>
  今日は日曜日</p>
  </div>
	</div>
</div>
</section>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

   <div class="box3">
	<div class="item3">
  <img src="img/blue_morning.jpg" alt="syujinkou">
   </div>
	   <div class="item3">
 	<p> いつも彼女が優しく起こしてくれる・・・<br/>
 	<br/>
 	 筈、<br/>
	<br/>
 	そう・・・<br/>
 	<br/>
 	昨日　些細なことで<br/>
	 ケンカをしてしまった。</p>
    </div>
  </div>
</div>
</section>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

  <div class="box5">
	<div class="item5">
	<img src="img/blue_morning_park.jpg" alt="waonimage_park">
   </div>
   <div class="item5">
  <p>外は平和に<br/>
  	犬や朝の散歩をする人たち<br/>
  	朝鳥の鳴く歌声に溢れている<br/>
  	<br/>
  	僕はというとブルーな朝</p>
   </div>
    </div>
</div>
</section>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

   <div class="box6">
	<div class="item6">
  <p>もう！<br/>
 	 卵切らしちゃった<br/>
  	パンももうすぐ無くなるわね・・・</p>
  </div>
  <div class="item6">
  <img src="img/blue_morning2.jpg" alt="hiroin">
   </div>
    </div>
</div>
</section>

    <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

    <div class="asaichi_box">
    <div class="asaichi">
    <p>朝市セール<br/><br/></p>
<img src="img/egg.jpg" alt="egg">
<p>取りたて 朝タマゴ　１０個<br/>
  	１２８円</p>
  	<a href="cart.jsp">ショッピングカートページへ</a>
    </div>
    <div class="asaichi">
    <p>朝市セール<br/><br/></p>
    <img src="img/pan.jpg" alt="pan">
  <p>モチもち！ワオン食パン<br/>
  	１４８円</p>
  	<a href="cart.jsp">ショッピングカートページへ</a>
  	<i class="fas fa-shopping-cart"></i>
  </div>
	</div>
</div>
</section>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

   <div class="box7">
	<div class="item7">
  <p>あいつ起きたかしら？<br/>
	  あいつったら、</p>
  	</div>
  <div class="item7">
   <img src="img/tokei.jpg" alt="waonimage_tokei">
   </div>
    </div>
</div>
</section>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

	<div class="box8">
	<div class="item8">
	<img src="img/blue_morning_phone.jpg" alt="waonimage_phon">
	 </div>
	 <div class="item8">
   <p>ピピッ!<br/>
   	毎朝、冷蔵庫アプリが<br/>
   	空の素材を自動で<br/>
   	教えてくれる。</p>
    </div>
    </div>
</div>
</section>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

   <div class="box9">
	<div class="item9">
	<img src="img/blue_morning_room.jpg" alt="waonimage_room">
	 </div>
	 <div class="item9">
   <p>さて、あやまって一緒に<br/>
	   いつもの買い物にでも行くか。</p>
    </div>
    </div>
</div>
</section>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

	 <div class="box10">
   <h3>　―　日曜日はいつものWAONへ　―</h3>

   <img src="img/waon.jpg" alt="waon_blue">

   <p>日曜朝市セール開催中！<br/><br/>
   9:00~11:00まで</p>
   </div>
</div>
</section>

 <section class="center">
  <div class="scroll-animation-obj scroll-animation-hop">

   <div class="witterbox">
   <div class="witterlink">
      <a href="witter.jsp" target="_brank">ツイートしよう！</a>
      </div>
	<div class="wittertext">
	<a href="witter.jsp" target="_brank"><img src="img/witter.png" alt="witter"></a>
	</div>
	</div>
</div>
</section>

  </article>
<footer class="copyright">
<p>copyright &copy; waon.co.jp</p>
</footer>

<p class="pagetop"><a href="#wrap">▲<br>PageTop</a></p>

</body>
</html>