$(document).ready(function() {

	var pagetop = $('.pagetop');

	$(window).scroll(function() {

		if ($(this).scrollTop() > 100) {

			pagetop.fadeIn();

		} else {

			pagetop.fadeOut();

		}

	});

	pagetop.click(function() {

		$('body, html').animate({
			scrollTop : 0
		}, 500);

		return false;

	});

});


$(function(){
	  const obj = $(".scroll-animation-obj");
	  const hopIn = $(".scroll-animation-hop");

	  $(window).on('scroll',function(){
	    obj.each(function(){
	      const objPos = $(this).offset().top;
	      const scroll = $(window).scrollTop();
	      const windowH = $(window).height();
	      if(scroll > objPos - windowH){
	        $(this).css({
	          'opacity': '1'
	        });
	      } else {
	        $(this).css({
	          'opacity': '0'
	        });
	      }
	    });

	    hopIn.each(function(){
	      const objPos = $(this).offset().top;
	      const scroll = $(window).scrollTop();
	      const windowH = $(window).height();
	      if(scroll > objPos - windowH){
	        $(this).css({
	          'transform': 'translate(0,0)'
	        });
	      } else {
	        $(this).css({
	          'transform': 'translate(0,60px)'
	        });
	      }
	    });

	  });
	});
